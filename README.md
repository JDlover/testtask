# Git flow
This chapter is going to explain you how to work with git in the team of your collegues. Such work is called <a href="glossary/gitflow.md" target="_blank">gitflow</a>.
It's supposed that you've already learned the basic git commands. If no, you can learn them from <a href="/git_basics.md">this</a> chapter.


**Notes:**
<br>_For your conveniency, the unknown words are explained. Just click on the unknown <a href="glossary/word.md" target="_blank">word</a>.
<br>Commands' names in this chapter are marked out like <mark>this</mark>._

## What to do

1. Use every <a href="glossary/IDE.md" target="_blank">IDE</a> you want;
<br>**Note:**
_If you don't have any projects in your IDE, clone one using <a href="glossary/VCS.md" target="_blank">VCS</a> option in your IDE._
1. 	Make the <mark>fetch</mark> of the changes to see the new branches, which might have been added by your collegues, while you were working on the other tasks.
1. Look through your _Issues_ in Gitlab and find the needed _Issue_. Read it.
1. 	<mark>Checkout</mark> the needed branch or create your own.
<br>**Tip:**
_If you are said to create your own branch, but they said nothing else, create your branch from the "develop" branch._
1. 	If you have checked out to a branch, that was created some time ago, make the <mark>pull</mark> to see the changes in a document, that your collegues might make. 
1. 	Edit a document.
1. 	<mark>Add</mark> your changes or set your IDE to always make this automatically.
1. 	<mark>Commit</mark> your changes (make a <a href="glossary/commit.md" target="_blank">commit</a>) and write a <a href="glossary/commit_ms.md" target="_blank">commit message</a>  necessarily.
1. 	Do the <mark>push</mark> to make the changes in your work version appear in the remote version and your collegues are able to see them.
1. Go to your _Issue_ and make a <a href="glossary/mr.md" target="_blank">merge request</a> (MR). Assign it to the responsible collegue, who checks your changes and adds them (merges) to the main branch (usually, _develop_, but it may be some intermediate branch, as well).
1. 	Wait while your changes will get to delivery. Meanwhile, you can move to the other task. To do this, repeat this instruction from the 2nd point.
